package com.example.RESTful.API.trial;

public record NewGreeting(long id, int age) {
}
