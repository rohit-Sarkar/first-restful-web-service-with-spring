package com.example.RESTful.API.trial;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    private static final String template = "Hello, %s!";
    private static final int newTemplate = 10;
    private final AtomicLong counter = new AtomicLong();
    //private String age;

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/newGreeting")
    public NewGreeting newGreeting(@RequestParam(value = "age", defaultValue = "0") int age) {
        return new NewGreeting(counter.incrementAndGet(), age);
    }
}
