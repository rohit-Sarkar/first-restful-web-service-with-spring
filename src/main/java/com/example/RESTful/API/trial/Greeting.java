package com.example.RESTful.API.trial;

public record Greeting(long id, String content) {
}
