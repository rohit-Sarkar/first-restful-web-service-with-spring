package com.example.RESTful.API.trial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResTfulApiTrialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResTfulApiTrialApplication.class, args);
	}

}
